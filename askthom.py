#!/usr/bin/env python3
# coding: utf8

"""askthom

A python CLI for a Thomson TG789vn gateway

This is mostly a learning exercise than a rational production code.
Structure inspired by https://vincent.bernat.ch

Features:
    - Get devices list

Warning:
    - Devices IP must comply to this pattern : `192.168.1.[60-129]` see #16

Author:     freezed <git@freezed.me> 2020-10-05
Licence:    GNU AGPL v3: http://www.gnu.org/licenses/
"""
import argparse
import logging
import logging.handlers
import os
import re
from subprocess import getstatusoutput
import sys

from bs4 import BeautifulSoup
import requests


logger = logging.getLogger(os.path.splitext(os.path.basename(sys.argv[0]))[0])


def setup_logging(options):
    """Configure logging"""
    root = logging.getLogger("")
    logger.setLevel(options.debug and logging.DEBUG or logging.INFO)

    if options.debug:
        # Logging with --debug option

        if not sys.stderr.isatty():
            # Logging in a file when launched not in a terminal

            facility = logging.handlers.SysLogHandler.LOG_DAEMON
            sysloghdlr = logging.handlers.SysLogHandler(
                address="/dev/log", facility=facility
            )
            sysloghdlr.setFormatter(
                logging.Formatter(
                    "{0}[{1}]: %(message)s".format(logger.name, os.getpid())
                )
            )
            root.addHandler(sysloghdlr)

        else:
            # Logging on terminal standard output

            consolehdlr = logging.StreamHandler()
            consolehdlr.setFormatter(
                logging.Formatter("%(levelname)s\t[%(name)s] %(message)s")
            )
            root.addHandler(consolehdlr)

    else:
        # Logging default setings

        consolehdlr = logging.StreamHandler()
        consolehdlr.setFormatter(logging.Formatter("%(message)s"))
        root.addHandler(consolehdlr)


class CustomFormatter(
    argparse.RawDescriptionHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
):
    """A nice CLI help using doctests"""


def parse_args(args=None):
    """Argument parser

    Put values & describe parameters here not in your code
    Ease usage with help & docs
    """
    # pylint see : https://satran.in/b/python--dangerous-default-value-as-argument
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(
        description=sys.modules[__name__].__doc__, formatter_class=CustomFormatter
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--debug",
        "-d",
        action="store_true",
        default=False,
        help="maximum logg rate for debugging",
    )

    group = parser.add_argument_group("askthom settings")
    group.add_argument(
        "--ssl",
        "-s",
        action="store_const",
        const="https",
        default="http",
        dest="protocol",
        help="Use https",
    )
    group.add_argument(
        "--only-online",
        "-o",
        action="store_const",
        const=True,
        default=False,
        dest="oonline",
        help="Keep only online devices",
    )
    group.add_argument(
        "--raw",
        "-r",
        action="store_const",
        const=True,
        default=False,
        dest="raw",
        help="Return raw data",
    )
    group.add_argument(
        "--address",
        "-a",
        default="192.168.1.254",
        help="Gateway address",
        metavar="IPV4",
        nargs="?",
        type=str,
    )
    group.add_argument(
        "--path",
        "-p",
        default="/cgi/b/devs/ov/",
        help="The path to reach",
        metavar="/PATH/TO/REACH/",
        nargs="?",
        type=str,
    )

    return parser.parse_args(args)


def main(options):
    """Function executed when executing this module as a script"""

    logger.debug("Ask Thom for known devices list @%s", options.address)

    devices = get_devices(options.protocol, options.address, options.path)

    if options.oonline:
        logger.debug("Ask Thom for online devices")

        devices = get_online_devices(devices)

        logger.debug(" -> %s devices online", len(devices))

    if options.raw:
        print(devices)

    else:
        logger.info("IP\t\t\tName\t\t\tInterface\tMAC")
        for dev in devices:
            logger.info(
                "%s\t%s\t%s\t%s",
                dev["ip"].ljust(14),
                dev["name"][:22].ljust(22),
                dev["interface"].ljust(8),
                dev["mac"],
            )


def get_devices(protocol, address, path):
    """
    Ask Thomsom for all devices known by the gateway

    Cannot scrap all parameter of a device, due to the very ugly HTML structure
    So we need to get :
        - All devices (name & MAC)q
        - All IPs
        - All interfaces (WLAN or ethport<n>)
    After scraping all this, we can merge all this in one object (`devices`)

    :Tests:
    These are online tests using defaults args to connect gateway

    >>> default_args = "http", "192.168.1.254", "/cgi/b/devs/ov/"
    >>> devices = get_devices(*default_args)
    >>> isinstance((devices), list)
    True
    >>> isinstance(devices[0], dict)
    True
    """

    url = "{p}://{a}{path}".format(p=protocol, a=address, path=path)
    payload = {"be": "0", "l0": "4", "l1": "0"}
    req = requests.get(url, params=payload)
    soup = BeautifulSoup(req.text, "html.parser")

    devices = [
        {
            "name": link.text,
            "mac": link.get("href").split(" ")[1].split("&key=")[1][:-1],
        }
        for link in soup.find_all("a")
        if len(link.get("href")) > 1
    ]
    logger.debug(" -> %s devices found", len(devices))

    # IP blocks targeted : 0.0.0.0 & 192.168.1.[60-129]
    ips = [
        ip.text
        for ip in soup.find_all(align="left")
        if re.fullmatch(r"(^192\.168\.1\.(1?[0-2]|[6-9])[0-9]$)|^(0\.){3}0$", ip.text)
    ]
    logger.debug(" -> %s IPs found", len(ips))

    interface = [
        ip.text[:-1] for ip in soup.find_all(align="left") if re.search("\n$", ip.text)
    ]
    logger.debug(" -> %s interfaces found", len(interface))

    i = 0
    for device in devices:
        device["ip"] = ips[i]
        device["interface"] = interface[i]
        i += 1

    return devices


def get_online_devices(devices):
    """Remove devices not online"""

    for device in devices.copy():
        if is_offline(device["ip"]):
            devices.remove(device)
            logger.debug("%s    [ OFFLINE ]", device["ip"].rjust(14))
        else:
            logger.debug("%s    --ON LINE--", device["ip"].rjust(14))

    return devices


def is_offline(ipv4):
    """Ping quietly & for 1 try the given IPv4 and return :
    - True for loss (is offline)
    - False for received (is online)
    """
    if ipv4 == "0.0.0.0":
        response = True
    else:
        response = bool(getstatusoutput("ping -qc 1 {target}".format(target=ipv4))[0])

    return response


if __name__ == "__main__":
    cli_options = parse_args()
    setup_logging(cli_options)
    main(cli_options)
