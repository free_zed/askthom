EDITOR = geany

help: # Print help on Makefile
	@grep '^[^.#]\+:\s\+.*#' Makefile | \
	sed "s/\(.\+\):\s*\(.*\) #\s*\(.*\)/`printf "\033[93m"`\1`printf "\033[0m"`	\3/" | \
	expand -t20

open_all: # Open all projects files
	${EDITOR} ${VIRTUAL_ENV}/bin/activate
	${EDITOR} .gitignore .gitlab-ci.yml askthom.py Makefile README.md requirements-dev.txt requirements.txt setup.cfg tests_askthom.py
	${EDITOR} .git/hooks/p*-commit
	${EDITOR} fixtures/vcr_cassettes*

pre_commit: # Run the pre-commit hook
	.git/hooks/pre-commit

clean: # Remove files not tracked in source control
	rm -rf htmlcov .pytest_cache
	find . -type f -name "*.orig" -delete
	find . -type f -name "*.pyc" -delete
	find . -type d -name "__pycache__" -delete
	find . -type d -empty -delete

lint: # Lint code
	${VIRTUAL_ENV}/bin/black --check --quiet .
	${VIRTUAL_ENV}/bin/flake8 --config=setup.cfg
	${VIRTUAL_ENV}/bin/pylint --rcfile=setup.cfg *.py

test: # Test for development
	${VIRTUAL_ENV}/bin/pytest --doctest-modules --exitfirst --failed-first --quiet --cov-report=term --cov=. *.py

test_ci: # Test for CI (no doctest & coverage)
	${VIRTUAL_ENV}/bin/pytest --quiet --cov=. --cov-report html --cov-report term:skip-covered

test_full: # Test with doctest & run code coverage report (Term & HTML)
	${VIRTUAL_ENV}/bin/pytest -vv --doctest-modules --cov-report=html --cov-report=term --cov=. *.py
