#!/usr/bin/env python3
# coding: utf8

"""test_askthom

Testing `askthom.py`

This is mostly a learning exercise than a rational production code.

Author:     freezed <git@freezed.me> 2020-10-05
Licence:    GNU AGPL v3: http://www.gnu.org/licenses/
"""
from os import environ
import logging

from pytest import fixture, mark
import re
import vcr

import askthom
from askthom import parse_args


@fixture
def default_args():
    return "http", "192.168.1.254", "/cgi/b/devs/ov/"


@mark.skipif(environ.get("CI", False) == "true", reason="Online test run not on CI")
@vcr.use_cassette("fixtures/vcr_cassettes-get_devices.yaml")
def test_get_devices(default_args):
    """Return a list of devices
    Devices are represented by dict"""

    # IP blocks targeted : 0.0.0.0 & 192.168.1.[60-129]
    ipv4_pattern = r"(^192\.168\.1\.(1?[0-2]|[6-9])[0-9]$)|^(0\.){3}0$"
    mac_pattern = r"^(([0-9]|[a-f]){2}:){5}([0-9]|[a-f]){2}$"
    interface_pattern = r"^ethport[0-4]$|^WLAN$|^unknown$"
    devices = askthom.get_devices(*default_args)

    assert len(devices[0]) == 4

    for k in ["mac", "name", "ip", "interface"]:
        assert k in devices[0].keys()

    for device in devices:
        assert re.fullmatch(ipv4_pattern, device["ip"])
        assert re.fullmatch(interface_pattern, device["interface"])
        assert re.fullmatch(mac_pattern, device["mac"])


# ###
# Testing parse_args()
# ###


def test_parse_args():
    """Defaults arguments are here"""

    cli_options = parse_args(["--debug", "--only-online", "--raw"])

    assert cli_options.debug
    assert cli_options.oonline
    assert cli_options.raw


# For the most used case (`parse_args()` with no arguments) we need to mock :
#   - `sys.argv`
class FakeSys:
    """Sys object mock
    An object faking the call to `sys`
    """

    argv = ["scriptname", "--debug", "--only-online", "--raw"]
    modules = {"askthom": "is it doc?"}


def mock_sys_all():
    """Function mock for sys.argv"""
    return FakeSys


def test_parse_args_mocked(monkeypatch):
    """Defaults arguments are here"""
    monkeypatch.setattr("askthom.sys", mock_sys_all())

    cli_options = parse_args()

    assert cli_options.debug
    assert cli_options.oonline
    assert cli_options.raw


# ###
# Testing setup_logging()
#
# Need to mock :
#   - `argparse` (the CLI argument parser)
# ###


class FakeArgparseOptionsDebug:
    """Argparse options object mock
    An object faking options returned by Argparse
    """

    debug = True


log_msg = "This is a log tests"


def test_setup_logging_debug(caplog):
    """Is the logger running as expected"""

    askthom.setup_logging(FakeArgparseOptionsDebug)
    askthom.logger.debug(log_msg)

    assert askthom.logger.level == logging.DEBUG
    assert log_msg in caplog.text


def test_setup_logging(caplog):
    """Is the logger running as expected"""
    askthom.setup_logging(FakeArgparseOptions)
    askthom.logger.debug(log_msg)

    assert askthom.logger.getEffectiveLevel() == logging.INFO
    assert log_msg not in caplog.text


# ###
# Testing main()
#
# Need to mock :
#   - `argparse` (the CLI argument parser)
#   - `get_devices` (providing the devices list known by the gateway)
# ###

PRETTY_LOGS = """INFO     pytest:askthom.py:171 IP			Name			Interface	MAC
INFO     pytest:askthom.py:178 0.0.0.0       	bar                   	i1      	fo:ob:ar
INFO     pytest:askthom.py:178 1.1.1.1       	baz                   	i2      	fo:ob:az
"""


class FakeArgparseOptions:
    """Argparse options object mock
    An object faking options returned by Argparse
    """

    address = "address"
    path = "path"
    protocol = "protocol"
    # sets options for all test cases :
    # {"test case name" : (value for oonline, value for raw)}
    test_cases = {
        "raw": (False, True),
        "pretty": (False, False),
        "ooline": (True, False),
    }
    debug = False

    def __init__(self, context):
        """Set an instance with options chosen for a test case"""

        self.oonline = self.test_cases[context][0]
        self.raw = self.test_cases[context][1]


def mock_argparse_options(test_case):
    """function mock"""
    return FakeArgparseOptions(test_case)


def mock_valid_get_devices(protocol, address, path):
    """get_devices() function mock """
    return [
        {"name": "bar", "interface": "i1", "mac": "fo:ob:ar", "ip": "0.0.0.0"},
        {"name": "baz", "interface": "i2", "mac": "fo:ob:az", "ip": "1.1.1.1"},
    ]


@mark.parametrize(
    "test_case, wtnss_stdout, wtnss_logs",
    [
        (
            "raw",
            "[{'name': 'bar', 'interface': 'i1', 'mac': 'fo:ob:ar', 'ip': '0.0.0.0'}, \
{'name': 'baz', 'interface': 'i2', 'mac': 'fo:ob:az', 'ip': '1.1.1.1'}]\n",
            "",
        ),
        (
            "pretty",
            "",
            PRETTY_LOGS,
        ),
    ],
)
def test_valid_main(monkeypatch, capsys, caplog, test_case, wtnss_stdout, wtnss_logs):
    """ Test main() - all devices"""

    monkeypatch.setattr("askthom.get_devices", mock_valid_get_devices)
    askthom.main(mock_argparse_options(test_case))
    captured = capsys.readouterr()
    assert captured.out == wtnss_stdout
    assert caplog.text == wtnss_logs


def mock_valid_get_online_devices(devices):
    """get_online_devices() function mock """
    return [
        {"name": "bar", "interface": "i1", "mac": "fo:ob:az", "ip": "1.1.1.1"},
    ]


def test_valid_main_only_online(monkeypatch, capsys):
    """ Test main() - only online device"""

    monkeypatch.setattr("askthom.get_devices", mock_valid_get_devices)
    monkeypatch.setattr("askthom.get_online_devices", mock_valid_get_online_devices)
    askthom.main(mock_argparse_options("ooline"))
    captured = capsys.readouterr()
    assert captured.out == ""


# ###
# Testing is_offline()
#
# Need to mock:
#   - `subprocess.getstatusoutput` : execute a shell command and return
#       (exitcode, output), here we use it to `ping` devices provided by `get_devices`
# ###


def mock_subprocess_getstatusoutput_exitcode_0(command):
    """`subprocess.getstatusoutput` function mock

    `ping` return exitcode 0 (device reachable)
    """

    return (0, "output not used here")


def mock_subprocess_getstatusoutput_exitcode_1(command):
    """`subprocess.getstatusoutput` function mock

    `ping` return exitcode 1 (device not reachable)
    """

    return (1, "output not used here")


def test_not_is_offline(monkeypatch):
    """the provided IPv4 is online (ping was received)"""
    monkeypatch.setattr(
        "askthom.getstatusoutput", mock_subprocess_getstatusoutput_exitcode_0
    )

    response = askthom.is_offline("This is an online IPV4")

    assert not response
    assert isinstance(response, bool)


@mark.parametrize("ipv4", ["0.0.0.0", "This is an offline IPV4"])
def test_is_offline(monkeypatch, ipv4):
    """the provided IPv4 is offline (ping was loss)"""
    monkeypatch.setattr(
        "askthom.getstatusoutput", mock_subprocess_getstatusoutput_exitcode_1
    )

    response = askthom.is_offline(ipv4)

    assert response
    assert isinstance(response, bool)


# ###
# Testing get_online_devices()
#
# Need to mock :
#   - `subprocess.getstatusoutput` in `is_offline()` (the call to `ping`)
# ###


def mock_subprocess_getstatusoutput_online(command):
    """`subprocess.getstatusoutput` function mock

    replace `ping` by a basic condition, if IPv4 in command is :
        - `x.x.x.42` or `127.0.0.0` => respond received
        - any other strings => respond loss
    """
    ping_response = (1, "output not used here")

    if command[-3:] == ".42" or command[-9:] == "127.0.0.0":
        ping_response = (0, "output not used here")

    return ping_response


def test_get_online_devices(monkeypatch):
    """The devices returned are considered on-line"""
    monkeypatch.setattr(
        "askthom.getstatusoutput", mock_subprocess_getstatusoutput_online
    )

    devices = askthom.get_online_devices(
        [
            {"name": "ze-2", "ip": "0.0.0.0"},
            {"name": "of-1", "ip": "1.1.4.54"},
            {"name": "on-2", "ip": "1.1.1.42"},
            {"name": "on-4", "ip": "1.1.1.42"},
            {"name": "lo-0", "ip": "127.0.0.0"},
            {"name": "on-1", "ip": "1.1.28.42"},
            {"name": "of-2", "ip": "1.1.34.75"},
            {"name": "of-4", "ip": "1.1.42.25"},
            {"name": "ze-4", "ip": "0.0.0.0"},
            {"name": "ze-1", "ip": "0.0.0.0"},
            {"name": "on-2", "ip": "1.1.76.42"},
            {"name": "lo-1", "ip": "127.0.0.1"},
            {"name": "on-3", "ip": "1.1.82.42"},
            {"name": "of-3", "ip": "1.1.12.49"},
            {"name": "of-5", "ip": "1.1.123.85"},
            {"name": "ze-3", "ip": "0.0.0.0"},
            {"name": "of-6", "ip": "1.1.81.54"},
        ]
    )

    assert devices == [
        {"name": "on-2", "ip": "1.1.1.42"},
        {"name": "on-4", "ip": "1.1.1.42"},
        {"name": "lo-0", "ip": "127.0.0.0"},
        {"name": "on-1", "ip": "1.1.28.42"},
        {"name": "on-2", "ip": "1.1.76.42"},
        {"name": "on-3", "ip": "1.1.82.42"},
    ]
