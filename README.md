askthom
=======

A python CLI for a [Thomson TG789vn gateway](https://fccid.io/RSE-TG789VN/User-Manual/User-Manual-1285790)


💡 Idea
-------

1. Give a try to the **Standalone Python script** proposed by _Vincent Bernat_ ( [🇫🇷](https://vincent.bernat.ch/fr/blog/2019-script-python-durable) | [🇬🇧](https://vincent.bernat.ch/en/blog/2019-sustainable-python-script) ).
1. Manage my gateway using CLI & `cron`
1. Using [frogg.it](https://lab.frogg.it) (_beta_)


✨ Features
-----------

- get devices list
    - raw or pretty print
        - all devices known
        - online devices (responding to `ping`)
- and [maybe more](https://lab.frogg.it/fcode/askthom/-/boards)…


🚀 (very) Quickstart
--------------------

```bash
git clone git@lab.frogg.it:fcode/askthom.git
cd askthom
echo "make --no-print-directory lint test" > .git/hooks/pre-commit
path/to/python3.7 -m venv ~/.venvs/askthom
source ~/.venvs/askthom/bin/activate
pip install -r requirements.txt
python askthom.py --help
```


🚧 Development
--------------

- _stable_ code is in `stable` branch (wow!)
- built with `Python 3.7`
- a `Makefile` with tools : run `make help` to have a look
- a minimal (but powerfull) `pre-commit` git hook


### 📌 Dependences

Details in [`requirements.txt`](requirements.txt) & [`requirements-dev.txt`](requirements-dev.txt)

- [`requests`](https://requests.readthedocs.io) (gateway is native `HTTP` speaker)
- [`beautifulsoup4`](https://pypi.org/project/beautifulsoup4/) (gateway talks a shitty `HTML`)


### ✅ Tests

- tools :
    * [`doctests`](https://docs.python.org/3/library/doctest.html) & [`pytest`](pytest.org/) : to get the job done
    * [`vcrpy`](https://vcrpy.readthedocs.io/en/latest/) to record http requests
- run : various test commands are listed in `make help`


### 🤝 Contributing

- Roadmap ➡️ [_project kanban_](https://lab.frogg.it/fcode/askthom/-/boards)
- Question, suggestion, issue ➡️ [_project issues_](https://lab.frogg.it/fcode/askthom/-/issues/new)
- Code submission  ➡️ [_project merge request_](https://lab.frogg.it/fcode/askthom/-/merge_requests/new)
